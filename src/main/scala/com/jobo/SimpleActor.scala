package com.jobo

import akka.actor.Actor
import akka.actor.Actor.Receive

class SimpleActor extends Actor {
  override def receive: Receive = {
    case _ => "Who"
  }
}
