package com.jobo

sealed trait Event

case class Login(username: String) extends Event
case class Logout(username: String) extends Event
case class GetChatLog(from: String) extends Event
case class ChatLog(log: List[String]) extends Event
case class ChatMessage(from: String, message: String) extends Event

